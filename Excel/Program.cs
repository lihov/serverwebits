﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel.Controller;
using Excel.Helpers;
using Excel.Model;

namespace Excel
{
    class Program
    {
        static void Main(string[] args)
        {
            Logger.Log.Info("--> Process started <--");

            var personsList = new List<Person>
            {
                new Person {Age = 18, Name = "User1", Surename = "Surename1", PhoneNumber = "+1 555-1234"},
                new Person {Age = 19, Name = "User2", Surename = "Surename2", PhoneNumber = "+1 555-1235"},
                new Person {Age = 20, Name = "User3", Surename = "Surename3", PhoneNumber = "+1 555-1236"}
            };

            try
            {
                var dataTableController = new DataTableController<Person>(ExcelHelper.WorksheetName);
                personsList.ForEach(p => dataTableController.Add(p));
                Logger.Log.Debug("DataTableController received initial data.");

                ExcelHelper.UpdateExcelFile(dataTableController.Table, ExcelHelper.WorkbookFileName);
            }
            catch (Exception ex)
            {
                Logger.Log.Error($"Program operation failed: {ex}");
            }
            finally
            {
                Logger.Log.Info("--> Process finished <--");
            }
            Console.ReadLine();
        }
    }
}
