﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Excel.Model
{
    public class Person
    {
        public int Age { get; set; }

        public string Surename { get; set; }

        public string Name { get; set; }

        public string PhoneNumber { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            var person = (Person)obj;
            if (ReferenceEquals(this, person))
            {
                return true;
            }
            
            return Age.Equals(person.Age) && 
                Surename.Equals(person.Surename) && 
                Name.Equals(person.Name) && 
                PhoneNumber.Equals(person.PhoneNumber);
        }

        public override int GetHashCode()
        {
            const int startHash = 32;
            var hash = startHash;
           
            hash ^= Age.GetHashCode();
            hash ^= Surename.GetHashCode();
            hash ^= Name.GetHashCode();
            hash ^= PhoneNumber.GetHashCode();

            return hash;
        }
    }
}
