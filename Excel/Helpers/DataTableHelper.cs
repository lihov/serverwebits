﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Drawing;

namespace Excel.Helpers
{
    public static class DataTableHelper
    {
        public static T GetObjectFromTableRow<T>(DataRow dataRow) where T : class, new()
        {
            var t = new T();
            dataRow.Table.Columns.Cast<DataColumn>().ToList().ForEach(c =>
            {
                var property = typeof(T).GetProperties().FirstOrDefault(p => p.Name == c.ColumnName && p.PropertyType == c.DataType);

                if (property != null && dataRow[c] != DBNull.Value)
                {
                    property.SetValue(t, dataRow[c], null);
                }
            });
            return t;
        }
    }
}
