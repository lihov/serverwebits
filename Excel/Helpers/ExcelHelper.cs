﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;

namespace Excel.Helpers
{
    public static class ExcelHelper
    {
        public const string WorkbookFileName = @"Persons.xlsx";
        public const string WorksheetName = @"Persons";

        public static void UpdateExcelFile(DataTable table, string fileName)
        {
            var workBook = new XLWorkbook();
            workBook.Worksheets.Add(table);
            workBook.SaveAs(fileName);
            Logger.Log.Info($"Data saved to file {fileName}");
        }
    }
}
