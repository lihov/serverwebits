﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Management.Instrumentation;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;
using Excel.Helpers;
using Excel.Model;

namespace Excel.Controller
{
    public class DataTableController<T> where T : class, new()
    {
        private readonly DataTable _dataTable;

        public DataTableController(string tableName)
        {
            if (string.IsNullOrEmpty(tableName))
            {
                throw new ArgumentException(@"Table name is not specified.");
            }
            var type = typeof(T);
            _dataTable = new DataTable {TableName = tableName};
            type.GetProperties().ToList().ForEach(x => _dataTable.Columns.Add(new DataColumn(x.Name, x.PropertyType)));
            if (_dataTable.Columns.Count == 0)
            {
                throw new ArgumentException($"Type {type.FullName} hasn't any properties. Data Table can't be build correctly.");
            }
            Logger.Log.Debug($"DataTable for type {type.FullName} successfully initialized.");
        }

        public void Add(T t)
        {
            var newRow = _dataTable.NewRow();
            t.GetType().GetProperties().ToList().ForEach(p =>
            {
                newRow[p.Name] = p.GetValue(t);
            });
            _dataTable.Rows.Add(newRow);
        }

        public bool Remove(T t)
        {
            var forDeleteRow = _dataTable.Rows.Cast<DataRow>()
                .FirstOrDefault(x => DataTableHelper.GetObjectFromTableRow<T>(x).Equals(t));
            if (forDeleteRow == null)
            {
                return false;
            }
            _dataTable.Rows.Remove(forDeleteRow);
            return true;
        }

        public DataTable Table => _dataTable.Copy();
    }
}
