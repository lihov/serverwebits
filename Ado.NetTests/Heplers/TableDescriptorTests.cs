﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ado.Net.Controller;
using Ado.Net.Helpers;
using Ado.Net.Model;
using NUnit.Framework;

namespace Ado.NetTests.Heplers
{
    [TestFixture]
    public class TableDescriptorTests
    {


        [Test]
        public void IdsAndRestTest()
        {
            Assert.AreEqual(@"ID", TableDescriptor<Product>.IdColumnDescriptor.ColumnName);
            Assert.AreEqual(@"@parameterID", TableDescriptor<Product>.IdColumnDescriptor.ParameterName);

            Assert.AreEqual(@"Name", TableDescriptor<Product>.NameColumnDescriptor.ColumnName);
            Assert.AreEqual(@"@parameterName", TableDescriptor<Product>.NameColumnDescriptor.ParameterName);

            Assert.AreEqual(@"ID", TableDescriptor<Category>.IdColumnDescriptor.ColumnName);
            Assert.AreEqual(@"@parameterID", TableDescriptor<Category>.IdColumnDescriptor.ParameterName);

            Assert.AreEqual(@"Name", TableDescriptor<Category>.NameColumnDescriptor.ColumnName);
            Assert.AreEqual(@"@parameterName", TableDescriptor<Category>.NameColumnDescriptor.ParameterName);

        }

        [Test]
        public void TableNameTest()
        {
            Assert.AreEqual("Products", TableDescriptor<Product>.TableName);
            Assert.AreEqual("Category", TableDescriptor<Category>.TableName);
        }
    }
}
