﻿using System;
using System.Diagnostics;
using System.Linq;
using Ado.Net.Controller;
using Ado.Net.Model;
using NUnit.Framework;

namespace Ado.NetTests.Controller
{
    [TestFixture]
    public class ShopDataAdapterTests
    {
        private ShopDataAdapter<Category> _categoryDataAdapter;
        private ShopDataAdapter<Product> _productDataAdapter;

        private Category _testCategory;
        private Product _testProduct;

        [SetUp]
        public void Init()
        {
            _categoryDataAdapter = new ShopDataAdapter<Category>();
            _productDataAdapter = new ShopDataAdapter<Product>();

            _testCategory = new Category
            {
                Name = @"Test Category",
            };

            _testProduct = new Product
            {
                Name = @"Test Product",
                CategoryId = 1,
                Price = 1.00M
            };
        }

        [TearDown]
        public void CleanUp()
        {
            _categoryDataAdapter?.Dispose();
            _productDataAdapter?.Dispose();
        }

        [Test]
        public void AddCategoryTest()
        {
            _categoryDataAdapter.Add(_testCategory);
            var addedCategory = _categoryDataAdapter[_testCategory.Name];

            Assert.IsNotNull(addedCategory);
            Assert.AreNotEqual(0, addedCategory.ID);

            _categoryDataAdapter.Remove(addedCategory);

            Assert.IsNull(_categoryDataAdapter[addedCategory.Name]);
        }

        [Test]
        public void AddProductTest()
        {
            _productDataAdapter.Add(_testProduct);
            var addedProduct = _productDataAdapter[_testProduct.Name];

            Assert.IsNotNull(addedProduct);
            Assert.AreNotEqual(0, addedProduct.ID);

            _productDataAdapter.Remove(addedProduct);

            Assert.IsNull(_productDataAdapter[addedProduct.Name]);
        }

        [Test]
        public void UpdateCategoryTest()
        {
            _categoryDataAdapter.Add(_testCategory);
            var addedCategory = _categoryDataAdapter[_testCategory.Name];

            Assert.IsNotNull(addedCategory);

            const string updatedCatName = @"updated test category name";
            addedCategory.Name = updatedCatName;

            _categoryDataAdapter.Update(addedCategory);

            var updatedCategory = _categoryDataAdapter[updatedCatName];

            Assert.AreEqual(updatedCatName, updatedCategory.Name);

            _categoryDataAdapter.Remove(updatedCategory);

            Assert.IsNull(_categoryDataAdapter[updatedCategory.Name]);
        }

        [Test]
        public void UpdateProductTest()
        {
            _productDataAdapter.Add(_testProduct);
            var addedProduct = _productDataAdapter[_testProduct.Name];

            Assert.IsNotNull(addedProduct);

            const string updatedProdName = @"updated test pruduct name";
            addedProduct.Name = updatedProdName;

            _productDataAdapter.Update(addedProduct);

            var updatedProduct = _productDataAdapter[updatedProdName];

            Assert.AreEqual(updatedProdName, updatedProduct.Name);

            _productDataAdapter.Remove(updatedProduct);

            Assert.IsNull(_productDataAdapter[updatedProduct.Name]);
        }

        [Test]
        public void RemoveCategoryTest()
        {
            _categoryDataAdapter.Add(_testCategory);
            var addedCategory = _categoryDataAdapter[_testCategory.Name];

            Assert.IsNotNull(addedCategory);

            var wrongCategory2 = new Category
            {
                ID = 99999,
                Name = "ddddddd"
            };

            _categoryDataAdapter.Remove(wrongCategory2);
            _categoryDataAdapter.Remove(addedCategory);

            Assert.IsNull(_categoryDataAdapter[addedCategory.Name]);
        }

        [Test]
        public void RemoveProductTest()
        {
            _productDataAdapter.Add(_testProduct);
            var addedProduct = _productDataAdapter[_testProduct.Name];

            Assert.IsNotNull(addedProduct);

            var wrongProduct2 = new Product
            {
                ID = 99999,
                Name = "ddddddd"
            };

            _productDataAdapter.Remove(wrongProduct2);
            _productDataAdapter.Remove(addedProduct);

            Assert.IsNull(_productDataAdapter[addedProduct.Name]);
        }

        [Test]
        public void PrintAllProductsTest()
        {
            var categories = _categoryDataAdapter.Items;
            var products = _productDataAdapter.Items;

            var fullProducts = products.Join(categories,
                p => p.CategoryId,
                c => c.ID,
                (p, c) => new FullProduct { ID = p.ID, Name = p.Name, Category = c, CategoryId = c.ID, Price = p.Price });

            fullProducts.ToList().ForEach(x =>
            {
                Debug.WriteLine($"{x.ID} {x.Name} {x.Price} {x.Category.Name}");
            });
        }

        [Test]
        public void CountTest()
        {
            Assert.AreEqual(4, _productDataAdapter.Count);
        }
    }
}