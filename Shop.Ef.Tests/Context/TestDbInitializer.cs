﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shop.Ef.Context;
using Shop.Ef.Model;

namespace Shop.Ef.Tests.Context
{
    internal class TestDbInitializer : DropCreateDatabaseAlways<ShopContext>
    {
        protected override void Seed(ShopContext context)
        {
            var processors = new Category { Name = "Процессоры" };
            var videos = new Category { Name = "Видеокарты" };
            var parts = new Category { Name = "Комплектующие" };
            var wholePcs = new Category { Name = "ПК в сборе" };

            var proc1 = new Product { Name = "Intel 1", Price = 100.00M };
            var proc2 = new Product { Name = "Intel 2", Price = 110.00M };

            var video1 = new Product { Name = "GeForce 1", Price = 50.00M };
            var video2 = new Product { Name = "GeForce 2", Price = 60.00M };

            var pc1 = new Product { Name = "PC1", Price = 200.00M };
            var pc2 = new Product { Name = "PC2", Price = 230.00M };

            var prodCats = new List<ProductCategory>
            {
                new ProductCategory {Product = proc1, Category = processors},
                new ProductCategory {Product = proc1, Category = parts},
                new ProductCategory {Product = proc2, Category = processors},
                new ProductCategory {Product = proc2, Category = parts},
                new ProductCategory {Product = video1, Category = videos},
                new ProductCategory {Product = video1, Category = parts},
                new ProductCategory {Product = video2, Category = videos},
                new ProductCategory {Product = video2, Category = parts},
                new ProductCategory {Product = pc1, Category = wholePcs},
                new ProductCategory {Product = pc2, Category = wholePcs}
            };

            context.ProductCategories.AddRange(prodCats);

            context.SaveChanges();

            var customer1 = new Customer { Email = "customer1@fmail.com", Name = "customer1", Phone = "555-821" };
            var customer2 = new Customer { Email = "customer2@fmail.com", Name = "customer2", Phone = "555-822" };
            var customer3 = new Customer { Email = "customer3@fmail.com", Name = "customer3", Phone = "555-822" };

            var order1 = new Order { Date = DateTime.UtcNow.AddDays(-1), Customer = customer1 };
            var order2 = new Order { Date = DateTime.UtcNow.AddDays(-2), Customer = customer2 };
            var order3 = new Order { Date = DateTime.UtcNow.AddDays(-3), Customer = customer3 };

            var prodOrds = new List<ProductOrder>
            {
                new ProductOrder {Order = order1, Product = proc1},
                new ProductOrder {Order = order1, Product = video1},
                new ProductOrder {Order = order2, Product = proc2},
                new ProductOrder {Order = order2, Product = video2},
                new ProductOrder {Order = order3, Product = video2}
            };

            context.ProductOrders.AddRange(prodOrds);

            context.SaveChanges();

            base.Seed(context);
        }
    }
}
