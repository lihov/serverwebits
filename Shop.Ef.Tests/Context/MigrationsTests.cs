﻿using System;
using System.Data.Entity;
using System.Linq;
using NUnit.Framework;
using Shop.Ef.Context;
using Shop.Ef.Migrations;

namespace Shop.Ef.Tests.Context
{
    [TestFixture]
    public class MigrationsTests
    {
        private ShopContext _dbContext;

        [OneTimeSetUp]
        public void Init()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ShopContext, Configuration>());
            _dbContext = new ShopContext();
        }

        [Test]
        public void MigrationTests()
        {
            var customer = _dbContext.Customers.FirstOrDefault(x => x.Name == "customer1");

            Assert.IsNotNull(customer);

            customer.BirthDay = DateTime.UtcNow.AddYears(-10);
            _dbContext.SaveChanges();

            customer = _dbContext.Customers.FirstOrDefault(x => x.Name == "customer1");

            Assert.IsNotNull(customer);
            Assert.IsNotNull(customer.BirthDay);

            Console.WriteLine(customer.BirthDay);
        }

        [OneTimeTearDown]
        public void Destruct()
        {
            _dbContext.Dispose();
        }
    }
}
