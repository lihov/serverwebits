﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using NUnit.Framework.Internal;
using Shop.Ef.Context;
using Shop.Ef.Model;

namespace Shop.Ef.Tests.Context
{
    [TestFixture]
    public class CrudTests
    {
        private ShopContext _dbContext;

        private const string TestName = "PC Xtream";
        private const string NewTestName = "PC Super";

        [OneTimeSetUp]
        public void Init()
        {
            Database.SetInitializer(new TestDbInitializer());
            _dbContext = new ShopContext();
        }

        [Test]
        public void AddEntryTest()
        {
            _dbContext.Products.Add(new Product {Name = TestName, Price = 350.00M});
            _dbContext.SaveChanges();

            Assert.AreEqual(TestName, _dbContext.Products.FirstOrDefault(x => x.Name == TestName)?.Name);
        }

        [Test]
        public void EditEntryTest()
        {
            var product = _dbContext.Products.FirstOrDefault(x => x.Name == TestName);

            Assert.NotNull(product);

            product.Name = NewTestName;
            _dbContext.SaveChanges();

            Assert.AreEqual(NewTestName, _dbContext.Products.FirstOrDefault(x => x.Name == NewTestName)?.Name);

            _dbContext.Entry(product).State = EntityState.Deleted;
            _dbContext.SaveChanges();

            Assert.IsNull(_dbContext.Products.FirstOrDefault(x => x.Name == NewTestName));
        }

        [OneTimeTearDown]
        public void Destruct()
        {
            _dbContext.Dispose();
        }
    }
}
