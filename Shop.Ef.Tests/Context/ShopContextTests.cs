﻿using System;
using System.Data.Entity;
using System.Linq;
using NUnit.Framework;
using Shop.Ef.Context;
using Shop.Ef.Model;
using Shop.Ef.Tests.Helpers;

namespace Shop.Ef.Tests.Context
{
    [TestFixture]
    public class ShopContextTests
    {
        private ShopContext _dbContext;

        [OneTimeSetUp]
        public void Init()
        {
            Database.SetInitializer(new TestDbInitializer());
            _dbContext = new ShopContext();
        }

        [Test]
        public void ClientSpendingTest()
        {
            var customersSpending = _dbContext.ProductOrders
                .Include(p => p.Product)
                .Include(p => p.Order)
                .GroupBy(p => p.Order.Customer)
                .Select(p => new {Customer = p.Key, Spending = p.Select(x => x.Product.Price).Sum()});

            Assert.AreEqual(150.00M, customersSpending.FirstOrDefault(x => x.Customer.Name == "customer1")?.Spending);
            Assert.AreEqual(170.00M, customersSpending.FirstOrDefault(x => x.Customer.Name == "customer2")?.Spending);
            Assert.AreEqual(60.00M, customersSpending.FirstOrDefault(x => x.Customer.Name == "customer3")?.Spending);

            customersSpending.ToList().ForEach(x => Console.WriteLine($"Customer: {x.Customer.Name} Spending: {x.Spending}"));
        }

        [Test]
        public void ProductMostFrequentlyBoughtTest()
        {
            var product = _dbContext.ProductOrders
                .Include(p => p.Product)
                .Include(p => p.Order)
                .GroupBy(p => p.Product)
                .Select(p => new {Item = p.Key, Count = p.Count()})
                .OrderByDescending(p => p.Count)
                .FirstOrDefault()?.Item;

            Assert.AreEqual("GeForce 2", product?.Name);

            Console.WriteLine($"Most frequently bought product: {product?.Name}");
        }

        [Test]
        public void BoughtProductsByCategoryTest()
        {
            var boughtCategories = _dbContext.ProductCategories
                .Include(p => p.Product)
                .Include(p => p.Category)
                .GroupBy(p => p.Category)
                .Select(p => new {Category = p.Key, BoughtCount = p.SelectMany(x => x.Product.ProductOrders).Count()});

            Assert.AreEqual(2, boughtCategories.FirstOrDefault(x => x.Category.Name == "Процессоры")?.BoughtCount);
            Assert.AreEqual(5, boughtCategories.FirstOrDefault(x => x.Category.Name == "Комплектующие")?.BoughtCount);
            Assert.AreEqual(3, boughtCategories.FirstOrDefault(x => x.Category.Name == "Видеокарты")?.BoughtCount);
            Assert.AreEqual(0, boughtCategories.FirstOrDefault(x => x.Category.Name == "ПК в сборе")?.BoughtCount);

            boughtCategories.ToList().ForEach(x => Console.WriteLine($"Category: {x.Category.Name} Bought Count: {x.BoughtCount}"));
        }

        [OneTimeTearDown]
        public void Destruct()
        {
            _dbContext.Dispose();
        }
    }
}