USE ItsEfShop;

DELETE FROM ProductCategories 
WHERE 
	CategoryId IN
	(
		SELECT CategoryId
		FROM ProductCategories
		INNER JOIN Categories ON ProductCategories.CategoryId = Categories.Id
		WHERE Name LIKE '%Вентилятор%'
	)

DELETE FROM Products WHERE Name LIKE '%Вентилятор%';

DELETE FROM Categories WHERE Name LIKE '%Вентилятор%';