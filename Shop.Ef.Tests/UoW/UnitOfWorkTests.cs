﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Ninject;
using NUnit.Framework;
using Shop.Ef.Context;
using Shop.Ef.IocUtils;
using Shop.Ef.Migrations;
using Shop.Ef.Model;
using Shop.Ef.Repository;
using Shop.Ef.UoW;

namespace Shop.Ef.Tests.UoW
{
    internal class CategoriesTestCases : IEnumerable
    {
        public IEnumerator GetEnumerator()
        {
            yield return new object[] { "Процессоры", 13 };
            yield return new object[] { "Комплектующие", 32 };
            yield return new object[] { "Видеокарты", 19 };
            yield return new object[] { "ПК в сборе", 0 };
        }
    }

    internal class CustomersTestCases : IEnumerable
    {
        public IEnumerator GetEnumerator()
        {
            yield return new object[] { "customer1", 600m };
            yield return new object[] { "customer2", 940m };
            yield return new object[] { "customer3", 960m };
        }
    }

    [TestFixture]
    public class UnitOfWorkTests
    {
        private DependencyResolver _dependencyResolver;

        private const string NewCategoryName = "TestFans";

        private const string NewProductName = "TestFan 1";

        private const string NewCategoryName2 = "TestOoo";

        private const string NewProductName2 = "TestOoo 1";


        [OneTimeSetUp]
        public void Init()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ShopContext, Configuration>());
            _dependencyResolver = new DependencyResolver(new StandardKernel());
        }

        [Order(1)]
        [TestCase(NewCategoryName, NewProductName)]
        [TestCase(NewCategoryName2, NewProductName2)]
        public void UnitOfWorkTest(string categoryName, string productName)
        {
            using (var uow = _dependencyResolver.GetService<IUnitOfWork>(
                new ConstructorArg("dependencyResolver", _dependencyResolver)))
            {
                var categoryRepo = uow.GetRepository<ICategoryRepository>();
                var productRepo = uow.GetRepository<IProductRepository>();
                var prodCatRepo = uow.GetRepository<IProductCategoryRepository>();

                var fanCat = new Category { Name = categoryName };
                categoryRepo.Create(fanCat);

                var fan1 = new Product { Name = productName, Price = 10.00M };
                productRepo.Create(fan1);

                prodCatRepo.Create(new ProductCategory { Category = fanCat, Product = fan1 });

                uow.Save();
            }
        }

        [Order(2)]
        [TestCase(NewCategoryName, NewProductName)]
        [TestCase(NewCategoryName2, NewProductName2)]
        public void UnitOfWorkRemoveTest(string categoryName, string productName)
        {
            using (var uow = _dependencyResolver.GetService<IUnitOfWork>(
                new ConstructorArg("dependencyResolver", _dependencyResolver)))
            {
                var categoryRepo = uow.GetRepository<ICategoryRepository>();
                var productRepo = uow.GetRepository<IProductRepository>();
                var prodCatRepo = uow.GetRepository<IProductCategoryRepository>();

                var cat = categoryRepo.GetAll().FirstOrDefault(x => x.Name == categoryName);
                var prod = productRepo.GetAll().FirstOrDefault(x => x.Name == productName);

                Assert.IsNotNull(cat);
                Assert.IsNotNull(prod);

                var prodCat = prodCatRepo.GetAll().Where(x => x.CategoryId == cat.Id);

                Assert.IsNotNull(prodCat);

                productRepo.Delete(prod);
                categoryRepo.Delete(cat);
                foreach (var productCategory in prodCat)
                {
                    prodCatRepo.Delete(productCategory);
                }

                uow.Save();
            }
        }

        [Order(3)]
        [TestCase(NewCategoryName, NewProductName)]
        [TestCase(NewCategoryName2, NewProductName2)]
        public void UnitOfWorkEntitiesRemovedTest(string categoryName, string productName)
        {
            using (var uow = _dependencyResolver.GetService<IUnitOfWork>(
                new ConstructorArg("dependencyResolver", _dependencyResolver)))
            {
                var categoryRepo = uow.GetRepository<ICategoryRepository>();
                var productRepo = uow.GetRepository<IProductRepository>();
                var prodCatRepo = uow.GetRepository<IProductCategoryRepository>();

                var cat = categoryRepo.GetAll().FirstOrDefault(x => x.Name == categoryName);
                var prod = productRepo.GetAll().FirstOrDefault(x => x.Name == productName);

                Assert.IsNull(cat);
                Assert.IsNull(prod);

                var prodCat = prodCatRepo.GetAll().Where(x => x.Category.Name == categoryName).ToList();

                Assert.AreEqual(0, prodCat.Count);
            }
        }

        [TestCase(1, 1)]
        [TestCase(1, 2)]
        [TestCase(2, 1)]
        public void GetByIdTest(int categoryId, int productId)
        {
            using (var uow = _dependencyResolver.GetService<IUnitOfWork>(
                new ConstructorArg("dependencyResolver", _dependencyResolver)))
            {
                var prodCatRepo = uow.GetRepository<IProductCategoryRepository>();

                var prodCat = prodCatRepo.GetById(categoryId, productId);

                Assert.IsNotNull(prodCat);
            }
        }

        [TestCaseSource(typeof(CategoriesTestCases))]
        public void GetBoughtProductsCountByCategoryTest(string categoryName, int quantity)
        {
            using (var uow = _dependencyResolver.GetService<IUnitOfWork>(
                new ConstructorArg("dependencyResolver", _dependencyResolver)))
            {
                var prodCatRepo = uow.GetRepository<IProductCategoryRepository>();
                Console.WriteLine($@"{categoryName} {prodCatRepo.GetBoughtProductsCountByCategory(categoryName)}");
                Assert.AreEqual(quantity, prodCatRepo.GetBoughtProductsCountByCategory(categoryName));
            }
        }


        [TestCaseSource(typeof(CustomersTestCases))]
        public void GetClientSpendingTest(string customerName, decimal spended)
        {
            using (var uow = _dependencyResolver.GetService<IUnitOfWork>(
                new ConstructorArg("dependencyResolver", _dependencyResolver)))
            {
                var prodCatRepo = uow.GetRepository<IProductOrderRepository>();
                Console.WriteLine($@"{customerName} {prodCatRepo.GetClientSpending(customerName)}");
                Assert.AreEqual(spended, prodCatRepo.GetClientSpending(customerName));
            }
        }

        [Test]
        public void GetProductMostFrequentlyBoughtTest()
        {
            using (var uow = _dependencyResolver.GetService<IUnitOfWork>(
                new ConstructorArg("dependencyResolver", _dependencyResolver)))
            {
                var prodCatRepo = uow.GetRepository<IProductOrderRepository>();
                Console.WriteLine($@"{prodCatRepo.GetProductMostFrequentlyBought()?.Name}");
                Assert.AreEqual(@"GeForce 2", prodCatRepo.GetProductMostFrequentlyBought()?.Name);
            }
        }

        [OneTimeTearDown]
        public void Destruct()
        {
            _dependencyResolver?.Dispose();
        }

    }
}
