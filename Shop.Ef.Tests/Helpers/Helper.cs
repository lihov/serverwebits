﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Ef.Tests.Helpers
{
    public static class Helper
    {
        private const int ColumnPadding = 20;


        internal static string Truncate(this string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) return value;
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }

        internal static void PrintTable<T>(this IEnumerable<T> table)
        {
            Console.WriteLine($"Table Name: {typeof(T).FullName}");
            var properties = typeof(T).GetProperties().ToList();

            properties.ForEach(x => Console.Write($"{x.Name.PadRight(ColumnPadding)}"));
            Console.WriteLine();
            Console.WriteLine(new string('-', ColumnPadding * properties.Count));
            
            table.ToList().ForEach(PrintEntity);
        }

        public static void PrintEntity<T>(T t) 
        {
            var properties = typeof(T).GetProperties().ToList();
            properties.ForEach(x =>
            {
                var value = x.GetValue(t, null);
                if (value.GetType().GetInterface(nameof(IEnumerable<T>)) != null)
                {
                    value = string.Empty;
                }
                Console.Write(value.ToString().Truncate(ColumnPadding).PadRight(ColumnPadding));
            });
            Console.WriteLine();
        }
    }
}
