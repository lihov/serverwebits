﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using NUnit.Framework.Internal;
using Shop.Ef.Context;
using Shop.Ef.Migrations;
using Shop.Ef.Model;
using Shop.Ef.Repository.Specific;

namespace Shop.Ef.Tests.Repository
{
    [TestFixture]
    public class RepositoryTests
    {
        private ShopContext _dbContext;

        [OneTimeSetUp]
        public void Init()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ShopContext, Configuration>());
            _dbContext = new ShopContext();
        }

        [Test]
        public void ProductOrderRepositoryTest()
        {
            //var productOrderRepo = new ProductOrderRepository(_dbContext);

            //var customer1Spending = productOrderRepo.GetClientSpending("customer1");
            //var customer2Spending = productOrderRepo.GetClientSpending("customer2");
            //var customer3Spending = productOrderRepo.GetClientSpending("customer3");

            //Assert.AreEqual(150.00M, customer1Spending);
            //Assert.AreEqual(170.00M, customer2Spending);
            //Assert.AreEqual(60.00M, customer3Spending);

            //Console.WriteLine($"customer1: {customer1Spending}");
            //Console.WriteLine($"customer2: {customer2Spending}");
            //Console.WriteLine($"customer3: {customer3Spending}");

            //var mostFreqBought = productOrderRepo.GetProductMostFrequentlyBought().Name;

            //Assert.AreEqual("GeForce 2", mostFreqBought);

            //Console.WriteLine($"Most fequently bought: {mostFreqBought}");
        }

        [Test]
        public void ProductCategoryRepositoryTest()
        {
            //var productCategoryRepo = new ProductCategoryRepository(_dbContext);

            //var pcbc1 = productCategoryRepo.GetBoughtProductsCountByCategory("Процессоры");
            //var pcbc2 = productCategoryRepo.GetBoughtProductsCountByCategory("Комплектующие");
            //var pcbc3 = productCategoryRepo.GetBoughtProductsCountByCategory("Видеокарты");
            //var pcbc4 = productCategoryRepo.GetBoughtProductsCountByCategory("ПК в сборе");

            //Assert.AreEqual(2, pcbc1);
            //Assert.AreEqual(5, pcbc2);
            //Assert.AreEqual(3, pcbc3);
            //Assert.AreEqual(0, pcbc4);

            //Console.WriteLine($"Процессоры {pcbc1}");
            //Console.WriteLine($"Комплектующие {pcbc2}");
            //Console.WriteLine($"Видеокарты {pcbc3}");
            //Console.WriteLine($"ПК в сборе {pcbc4}");
        }

        [Test]
        public void CategoryRepositoryTest()
        {
            //var categoryRepo = new CategoryRepository(_dbContext);

            //const string testCategoryName = "TestCategory";
            //const string newTestCategoryName = "NewTestCategory";

            //categoryRepo.Create(new Category{Name = testCategoryName});
            //categoryRepo.Save();

            //var testCategory = categoryRepo.GetAll().FirstOrDefault(x => x.Name == testCategoryName);

            //Assert.IsNotNull(testCategory);
            //Assert.AreEqual(testCategoryName, testCategory.Name);
            //Assert.IsNotNull(categoryRepo.GetById(testCategory.Id));

            //testCategory.Name = newTestCategoryName;

            //categoryRepo.Update(testCategory);
            //categoryRepo.Save();

            //Assert.AreEqual(newTestCategoryName, categoryRepo.GetById(testCategory.Id).Name);

            //categoryRepo.Delete(testCategory);
            //categoryRepo.Save();

            //Assert.IsNull(categoryRepo.GetById(testCategory.Id));
        }

        [OneTimeTearDown]
        public void Destruct()
        {
            _dbContext.Dispose();
        }
    }
}
