﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson01Config
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.Out.WriteLine($"Site URL: {ConfigurationManager.AppSettings[SettingsHelper.SiteUrlKey]}");
                throw new Exception("Test system exception");
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine($"Setting {SettingsHelper.SiteUrlKey} reading error: {ex.Message}");

                #region DebugOutput
#if DEBUG
                Debug.WriteLine(ex);
#endif
                #endregion
            }
            Console.ReadLine();
        }
    }
}
