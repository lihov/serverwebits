﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Countries
{
    public static class Helper
    {
        public const int LeftPaddingVal = 50;

        public const int HeaderLenght = 110;

        public const string VerticalSplitter = " | ";

        public const char HorizontalSplitter = '-';

        public static int CurrencyEntryTailLength => VerticalSplitter.Length;
    }
}
