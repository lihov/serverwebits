﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Countries
{
    class Program
    {
        static void Main(string[] args)
        {

            dynamic countries = JArray.Parse(File.ReadAllText("Countries.txt"));

            Console.Out.WriteLine($"Summary population of American region: {((IEnumerable<dynamic>)countries).Sum(x => (int)x.population)}");
            Console.Out.WriteLine();

            Console.Out.WriteLine($"{"Countries:".PadLeft(Helper.LeftPaddingVal)}{Helper.VerticalSplitter}Currencies:");
            Console.Out.WriteLine(new string(Helper.HorizontalSplitter, Helper.HeaderLenght));
            ((IEnumerable<dynamic>)countries).ToList().ForEach(c =>
            {
                var sb = new StringBuilder();
                ((IEnumerable<dynamic>)c.currencies).ToList().ForEach(x => sb.Append($"{x.name}{Helper.VerticalSplitter}"));
                var currStr = sb.ToString();
                
                Console.Out.WriteLine($"{c.name.ToString().PadLeft(Helper.LeftPaddingVal)}{Helper.VerticalSplitter}" +
                                      $"{currStr.ToString().Remove(currStr.Length - Helper.CurrencyEntryTailLength)}");
            });
            
            Console.ReadLine();
        }
    }
}
