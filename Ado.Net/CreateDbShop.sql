USE master;
IF EXISTS(select * from sys.databases where name='ItsWebShop')
BEGIN
	DROP DATABASE [ItsWebShop]
END
GO

CREATE DATABASE [ItsWebShop];

USE ItsWebShop;
GO

CREATE TABLE [dbo].[Category]
(
	[ID] INT IDENTITY(1, 1) NOT NULL,
	[Name] NVARCHAR(150) NOT NULL,
	CONSTRAINT [category_primary_id] PRIMARY KEY ([ID])
);
GO

CREATE TABLE [dbo].[Products]
(
	[ID] INT IDENTITY(1, 1) NOT NULL,
	[Name] NVARCHAR(150) NOT NULL,
	[Price] MONEY NOT NULL,
	[CategoryId] INT NOT NULL,
	CONSTRAINT [products_primary_id] PRIMARY KEY ([ID]),
	CONSTRAINT [foreign_category] FOREIGN KEY ([CategoryId])
		REFERENCES [dbo].[Category] ([ID])
);
GO

CREATE INDEX ix_categoryid ON [dbo].[Products] ([CategoryId]);
GO

INSERT INTO [dbo].[Category] VALUES (N'Sausage products')
INSERT INTO [dbo].[Category] VALUES (N'Bread products')
INSERT INTO [dbo].[Category] VALUES (N'Meat products')

INSERT INTO [dbo].[Products] VALUES (N'Sausege1', 45.95, 1)
INSERT INTO [dbo].[Products] VALUES (N'Sausege2', 49.99, 1)
INSERT INTO [dbo].[Products] VALUES (N'Braed1', 2.99, 2)
INSERT INTO [dbo].[Products] VALUES (N'Hamon1', 76.99, 3)
GO