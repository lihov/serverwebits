﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ado.Net.Model
{
    public class FullProduct : Product
    {
        public Category Category { get; set; }

        public override string ToString()
        {
            const int padding = 15;
            return $"ID: {ID.ToString().PadRight(padding)} | Name: {Name.PadRight(padding)} | " +
                   $"Price: {Price.ToString(CultureInfo.InvariantCulture).PadRight(padding)} | " +
                   $"Category: {Category.Name.PadRight(padding)}";
        }
    }
}
