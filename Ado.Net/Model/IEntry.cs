﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ado.Net.Model
{
    public interface IEntry
    {
        int ID { get; set; }
        string Name { get; set; }
    }
}
