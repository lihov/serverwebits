﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ado.Net.Attributes;

namespace Ado.Net.Model
{
    [TableInfo(TableName = "Category")]
    public class Category : IEntry
    {
        [ColumnInfo(ColumnDbType = SqlDbType.Int)]
        public int ID { get; set; }

        [ColumnInfo(ColumnDbType = SqlDbType.NVarChar)]
        public string Name { get; set; }
    }
}
