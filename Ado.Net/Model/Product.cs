﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ado.Net.Attributes;

namespace Ado.Net.Model
{
    [TableInfo(TableName = "Products")]
    public class Product : IEntry
    {
        [ColumnInfo(ColumnDbType = SqlDbType.Int)]
        public int ID { get; set; }

        [ColumnInfo(ColumnDbType = SqlDbType.NVarChar)]
        public string Name { get; set; }

        [ColumnInfo(ColumnDbType = SqlDbType.Money)]
        public decimal Price { get; set; }

        [ColumnInfo(ColumnDbType = SqlDbType.Int)]
        public int CategoryId { get; set; }
    }
}
