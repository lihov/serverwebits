﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using Ado.Net.Attributes;
using Ado.Net.Model;
using Ado.Net.Helpers;
using Excel.Helpers;

namespace Ado.Net.Controller
{
    public class ShopDataAdapter<T> : IDisposable where T : class, IEntry, new()
    {
        private readonly SqlConnection _sqlConnection;

        public ShopDataAdapter()
        {
            var connectionString = ConfigurationManager.ConnectionStrings[Helper.ConnectionStringSectionName].ConnectionString;
            _sqlConnection = new SqlConnection(connectionString);
            _sqlConnection.Open();
        }


        private static void AssertSqlConnected(SqlConnection sqlConnection)
        {
            if (sqlConnection?.State != ConnectionState.Open)
            {
                throw new InvalidOperationException("Sql Server isn't connected");
            }
        }

        private static void AssertParamExists(IEntry entry)
        {
            if (entry == null)
            {
                throw new ArgumentException("Entry is null");
            }
        }

        private static void AssertIdSpecified(IEntry entry)
        {
            if (entry?.ID < 1)
            {
                throw new ArgumentException("Incorrect entry ID specified");
            }
        }



        private static void ExecuteCommand(string sql, T t, SqlConnection sqlConnection)
        {
            AssertSqlConnected(sqlConnection);
            AssertParamExists(t);
            using (var command = new SqlCommand(sql, sqlConnection))
            {
                Helper.AddParametersToCommand(command, t);
                command.ExecuteNonQuery();
            }
        }

        public int Count
        {
            get
            {
                AssertSqlConnected(_sqlConnection);
                var sql = $"SELECT COUNT(*) FROM {TableDescriptor<T>.TableName}";
                using (var command = new SqlCommand(sql, _sqlConnection))
                {
                    return (int)command.ExecuteScalar();
                }
            }
        }

        public void Add(T t)
        {
            var columns = string.Join(", ",
                TableDescriptor<T>.ColumnsDescriptors.Where(c => c.ColumnName != Helper.IdPropertyId).Select(c => c.ColumnName));

            var parameters = string.Join(", ",
                TableDescriptor<T>.ColumnsDescriptors.Where(c => c.ColumnName != Helper.IdPropertyId).Select(c => c.ParameterName));

            var sql = $"INSERT INTO {TableDescriptor<T>.TableName}({columns}) VALUES({parameters})";
            ExecuteCommand(sql, t, _sqlConnection);
        }

        public void Update(T t)
        {
            AssertIdSpecified(t);
            var columnsEqualsParameters = string.Join(", ", TableDescriptor<T>.ColumnsDescriptors
                .Where(c => c.ColumnName != Helper.IdPropertyId).Select(x => $"{x.ColumnName} = {x.ParameterName}"));

            var sql = $"UPDATE {TableDescriptor<T>.TableName} SET {columnsEqualsParameters} " +
                $"WHERE {TableDescriptor<T>.IdColumnDescriptor.ColumnName} = {TableDescriptor<T>.IdColumnDescriptor.ParameterName}";
            ExecuteCommand(sql, t, _sqlConnection);
        }

        public void Remove(T t)
        {
            AssertIdSpecified(t);
            var sql = $"DELETE FROM {TableDescriptor<T>.TableName} " +
                      $"WHERE {TableDescriptor<T>.IdColumnDescriptor.ColumnName} = {TableDescriptor<T>.IdColumnDescriptor.ParameterName}";
            ExecuteCommand(sql, t, _sqlConnection);
        }

        public IEnumerable<T> Items
        {
            get
            {
                AssertSqlConnected(_sqlConnection);
                var sql = $"SELECT * FROM {TableDescriptor<T>.TableName}";
                return Helper.GetDataFromDb<T>(sql, _sqlConnection, null).ToList();
            }
        }

        public T this[string name]
        {
            get
            {
                AssertSqlConnected(_sqlConnection);
                if (name == null)
                {
                    return null;
                }
                var sql = $"SELECT * FROM {TableDescriptor<T>.TableName} " +
                          $"WHERE {TableDescriptor<T>.NameColumnDescriptor.ColumnName} = {TableDescriptor<T>.NameColumnDescriptor.ParameterName}";
                return Helper.GetDataFromDb(sql, _sqlConnection, new T {Name = name}).FirstOrDefault();
            }
        }

        public T this[int id]
        {
            get
            {
                AssertSqlConnected(_sqlConnection);
                if (id < 1)
                {
                    return null;
                }
                var sql = $"SELECT * FROM {TableDescriptor<T>.TableName} " +
                          $"WHERE {TableDescriptor<T>.IdColumnDescriptor.ColumnName} = {TableDescriptor<T>.IdColumnDescriptor.ParameterName}";
                return Helper.GetDataFromDb(sql, _sqlConnection, new T { ID = id }).FirstOrDefault();
            }
        }

        public void Dispose()
        {
            _sqlConnection.Dispose();
        }
    }
}
