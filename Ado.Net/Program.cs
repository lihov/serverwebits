﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Ado.Net.Controller;
using Ado.Net.Helpers;
using Ado.Net.Model;

namespace Ado.Net
{

    class Program
    {
        private static  readonly string ConnectionString = 
            ConfigurationManager.ConnectionStrings[Helper.ConnectionStringSectionName].ConnectionString;

        private static void PrintAllProducts()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                Helper.GetFullProducts(connection).ForEach(Console.WriteLine);
            }   
        }

        private static void CategoryAddTransaction(string categoryName)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                try
                {
                    const string sql = "INSERT INTO Category(Name) VALUES(@categoryName)";
                    var command = new SqlCommand(sql, connection) {Transaction = transaction};
                    command.Parameters.Add(
                        new SqlParameter("@categoryName", categoryName) {SqlDbType = SqlDbType.NVarChar});
                    command.ExecuteNonQuery();
                    throw new Exception();
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }
            }
        }

        static void Main(string[] args)
        {

            CategoryAddTransaction("Test Category1");
            PrintAllProducts();
            Console.ReadLine();
        }
    }
}
