﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ado.Net.Helpers
{
    public class ColumnDescriptor
    {
        public string ColumnName { get; set; }

        public string ParameterName { get; set; }

        public SqlDbType ColumnType { get; set; }
    }
}
