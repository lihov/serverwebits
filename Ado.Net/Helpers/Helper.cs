﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using Ado.Net.Attributes;
using Ado.Net.Model;
using Excel.Helpers;

namespace Ado.Net.Helpers
{
    public static class Helper
    {
        public const string ConnectionStringSectionName = @"ItsWebShopConnection";
        public const string ParameterId = @"parameter";
        public const string IdPropertyId = @"ID";
        public const string NamePropertyId = @"Name";

        public static object GetDefault(Type type)
        {
            if (type.IsValueType)
            {
                return Activator.CreateInstance(type);
            }
            return type == typeof(string) ? string.Empty : null;
        }

        public static SqlDbType GetSqlDbType(PropertyInfo property)
        {
            return property?.GetCustomAttributes(true).OfType<ColumnInfoAttribute>()
                .FirstOrDefault()?.ColumnDbType ??
                throw new InvalidEnumArgumentException(
                "Invalid columm SQL type from model attribute obatined");
        }

        public static void AddParametersToCommand<T>(SqlCommand command, T t)
        {
            typeof(T).GetProperties().ToList().ForEach(p =>
            {
                command.Parameters.Add(
                    new SqlParameter(GetParameterNameFromTemplate(p.Name), p.GetValue(t) ?? 
                    GetDefault(p.PropertyType)) { SqlDbType = GetSqlDbType(p) });
            });
        }

        public static IEnumerable<T> GetDataFromDb<T>(string sql, SqlConnection sqlConnection, T t) where T : class, IEntry, new() 
        {
            using (var command = new SqlCommand(sql, sqlConnection))
            {
                if (t != null)
                {
                    AddParametersToCommand(command, t);
                }

                using (var reader = command.ExecuteReader())
                {
                    var dataTable = new DataTable();
                    dataTable.Load(reader);
                    return dataTable.Rows.Cast<DataRow>().Select(DataTableHelper.GetObjectFromTableRow<T>).ToList();
                }
            }
        }

        public static string GetParameterNameFromTemplate(string name)
        {
            return $"@{ParameterId}{name}";
        }

        public static List<FullProduct> GetFullProducts(SqlConnection sqlConnection)
        {
            if (sqlConnection?.State != ConnectionState.Open)
            {
                throw new InvalidOperationException("Sql Server isn't connected");
            }
            var categoryAdapter = new SqlDataAdapter(@"SELECT * FROM Category", sqlConnection);
            var productAdapter = new SqlDataAdapter(@"SELECT * FROM Products", sqlConnection);

            var fullProductsDataSet = new DataSet();
            categoryAdapter.Fill(fullProductsDataSet, @"Category");
            productAdapter.Fill(fullProductsDataSet, @"Products");

            var relation = fullProductsDataSet.Relations.Add(@"productCategory",
                fullProductsDataSet.Tables[@"Category"].Columns[@"ID"],
                fullProductsDataSet.Tables[@"Products"].Columns[@"CategoryId"]);

            return fullProductsDataSet.Tables[@"Products"].Rows.Cast<DataRow>()
                .Select(r =>
                {
                    var product = DataTableHelper.GetObjectFromTableRow<FullProduct>(r);
                    product.Category = DataTableHelper.GetObjectFromTableRow<Category>(r.GetParentRow(relation));
                    return product;
                }).ToList();
        }
    }
}
