﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Ado.Net.Attributes;
using Ado.Net.Model;

namespace Ado.Net.Helpers
{
    public static class TableDescriptor<T> where T : class, IEntry, new()
    {
        public static string TableName => typeof(T).GetCustomAttributes(true)
                                                .OfType<TableInfoAttribute>().FirstOrDefault()?.TableName;

        public static ColumnDescriptor IdColumnDescriptor
        {
            get
            {
                var idProperty = typeof(T).GetProperties().FirstOrDefault(p => p.Name == Helper.IdPropertyId);
                return new ColumnDescriptor
                {
                    ColumnName = idProperty?.Name,
                    ParameterName = Helper.GetParameterNameFromTemplate(idProperty?.Name),
                    ColumnType = Helper.GetSqlDbType(idProperty)
                };
            }
        }

        public static ColumnDescriptor NameColumnDescriptor
        {
            get
            {
                var nameProperty = typeof(T).GetProperties().FirstOrDefault(p => p.Name == Helper.NamePropertyId);
                return new ColumnDescriptor
                {
                    ColumnName = nameProperty?.Name,
                    ParameterName = Helper.GetParameterNameFromTemplate(nameProperty?.Name),
                    ColumnType = Helper.GetSqlDbType(nameProperty)
                };
            }
        }

        public static IEnumerable<ColumnDescriptor> ColumnsDescriptors
        {
            get
            {
                return typeof(T).GetProperties().Select(p => 
                    new ColumnDescriptor
                    {
                        ColumnName = p.Name,
                        ParameterName = Helper.GetParameterNameFromTemplate(p.Name),
                        ColumnType = Helper.GetSqlDbType(p)
                    });
            }
        }
    }
}
