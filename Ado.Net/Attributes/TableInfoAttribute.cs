﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ado.Net.Attributes
{
    public class TableInfoAttribute : Attribute
    {
        public string TableName { get; set; }
    }
}
