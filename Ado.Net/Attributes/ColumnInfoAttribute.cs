﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ado.Net.Attributes
{
    public class ColumnInfoAttribute : Attribute
    {
        public SqlDbType ColumnDbType { get; set; }
    }
}
