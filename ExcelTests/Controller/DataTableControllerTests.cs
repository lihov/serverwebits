﻿using System.Collections;
using System.Data;
using System.Linq;
using Excel.Controller;
using Excel.Model;
using NUnit.Framework;
using Excel.Helpers;

namespace ExcelTests.Controller
{
    [TestFixture]
    public class DataTableControllerTests
    {
        private DataTableController<Person> _dataTableController;

        [SetUp]
        public void Init()
        {
            _dataTableController = new DataTableController<Person>(ExcelHelper.WorksheetName);
        }

        [Test]
        public void DataTableControllerTest()
        {
            
            Assert.AreEqual(ExcelHelper.WorksheetName, _dataTableController.Table.TableName);

            var dataColumns = _dataTableController.Table.Columns.Cast<DataColumn>().ToList();
            typeof(Person).GetProperties().ToList().ForEach(p =>
            {
                Assert.AreEqual(true, dataColumns.Select(x => x.ColumnName).Contains(p.Name));
                Assert.AreEqual(true, dataColumns.Select(x => x.DataType).Contains(p.PropertyType));
            });
        }

        [Test]
        public void AddTest()
        {
            var newPerson = new Person
            {
                Age = 10,
                Name = "Person1",
                PhoneNumber = "123",
                Surename = "Surename1"
            };

            Assert.IsNull(_dataTableController.Table.Rows.Cast<DataRow>()
                .FirstOrDefault(x => DataTableHelper.GetObjectFromTableRow<Person>(x).Equals(newPerson)));

            _dataTableController.Add(newPerson);

            Assert.IsNotNull(_dataTableController.Table.Rows.Cast<DataRow>()
                .FirstOrDefault(x => DataTableHelper.GetObjectFromTableRow<Person>(x).Equals(newPerson)));
        }

        [Test]
        public void RemoveTest()
        {
            var newPerson = new Person
            {
                Age = 102,
                Name = "Person2",
                PhoneNumber = "123",
                Surename = "Surename2"
            };

            Assert.IsNull(_dataTableController.Table.Rows.Cast<DataRow>()
                .FirstOrDefault(x => DataTableHelper.GetObjectFromTableRow<Person>(x).Equals(newPerson)));

            _dataTableController.Add(newPerson);
            Assert.IsTrue(_dataTableController.Remove(newPerson));

            Assert.IsNull(_dataTableController.Table.Rows.Cast<DataRow>()
                .FirstOrDefault(x => DataTableHelper.GetObjectFromTableRow<Person>(x).Equals(newPerson)));
        }
    }
}