﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Ef.Helpers
{
    public static class Helper
    {
        public const string DefaultConnectionString = @"ItsWebShopConnection";
        public const int MaxFieldLength = 100;
        public const int MaxShortFieldLength = 30;
    }
}
