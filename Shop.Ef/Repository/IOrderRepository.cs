﻿using Shop.Ef.Model;

namespace Shop.Ef.Repository
{
    public interface IOrderRepository : IRepository<Order>
    {
    }
}
