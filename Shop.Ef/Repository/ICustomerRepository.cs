﻿using Shop.Ef.Model;

namespace Shop.Ef.Repository
{
    public interface ICustomerRepository : IRepository<Customer>
    {
    }
}
