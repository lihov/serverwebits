﻿using Shop.Ef.Model;

namespace Shop.Ef.Repository
{
    public interface IProductCategoryRepository : IRepository<ProductCategory>
    {
        int GetBoughtProductsCountByCategory(string categoryName);
    }
}
