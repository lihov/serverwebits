﻿using System;
using System.Data.Entity;
using System.Linq;

namespace Shop.Ef.Repository
{
    public class BaseEfRepository<T> : IRepository<T> where T: class
    {
        protected DbContext DbContext;

        protected DbSet<T> DbSet;

        public BaseEfRepository(DbContext dbContext)
        {
            DbContext = dbContext;
            DbSet = DbContext.Set<T>();
        }

        public virtual void Create(T entity)
        {
            DbSet.Add(entity);
        }

        public virtual void Update(T entity)
        {
            DbSet.Attach(entity);
            DbContext.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Delete(T entity)
        {
            if (DbContext.Entry(entity).State == EntityState.Detached)
            {
                DbSet.Attach(entity);
            }
            DbSet.Remove(entity);
        }

        public virtual T[] GetAll()
        {
            return DbSet.Cast<T>().ToArray();
        }

        public virtual T GetById(params int[] id)
        {
            return DbSet.Find(id.Cast<object>().ToArray());
        }

        public virtual void Save()
        {
            DbContext.SaveChanges();
        }
    }
}
