﻿using Shop.Ef.Model;

namespace Shop.Ef.Repository
{
    public interface IProductRepository : IRepository<Product>
    {
    }
}
