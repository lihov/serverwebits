﻿using Shop.Ef.Model;

namespace Shop.Ef.Repository
{
    public interface IProductOrderRepository : IRepository<ProductOrder>
    {
        decimal GetClientSpending(string clientName);

        Product GetProductMostFrequentlyBought();
    }
}
