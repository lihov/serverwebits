﻿using System.Data.Entity;
using Shop.Ef.Context;
using Shop.Ef.Model;

namespace Shop.Ef.Repository.Specific
{
    public class CategoryRepository : BaseEfRepository<Category>, ICategoryRepository
    {
        public CategoryRepository(DbContext dbContext) : base(dbContext)
        {
        }
    }
}
