﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shop.Ef.Context;
using Shop.Ef.Model;

namespace Shop.Ef.Repository.Specific
{
    public class ProductOrderRepository : BaseEfRepository<ProductOrder>, IProductOrderRepository
    {
        public decimal GetClientSpending(string clientName)
        {
            return DbSet
                .Where(p => p.Order.Customer.Name == clientName)
                .Sum(p => p.Product.Price * p.Quantity);
        }

        public Product GetProductMostFrequentlyBought()
        {
            return DbSet
                .GroupBy(p => p.Product)
                .OrderByDescending(p => p.Sum(q => q.Quantity))
                .FirstOrDefault()?.Key;
        }

        public ProductOrderRepository(DbContext dbContext) : base(dbContext)
        {
        }
    }
}
