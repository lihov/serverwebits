﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shop.Ef.Context;
using Shop.Ef.Model;

namespace Shop.Ef.Repository.Specific
{
    public class ProductCategoryRepository : BaseEfRepository<ProductCategory>, IProductCategoryRepository
    {
        public int GetBoughtProductsCountByCategory(string categoryName)
        {
            return DbSet
                .Where(p => p.Category.Name == categoryName)
                .SelectMany(p => p.Product.ProductOrders)
                //TODO не ясно как лучше сделать (это сильно страшно?)
                .ToList()
                .Sum(p => p.Quantity);
        }

        public ProductCategoryRepository(DbContext dbContext) : base(dbContext)
        {
        }
    }
}
