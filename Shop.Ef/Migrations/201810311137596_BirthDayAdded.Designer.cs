// <auto-generated />
namespace Shop.Ef.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class BirthDayAdded : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(BirthDayAdded));
        
        string IMigrationMetadata.Id
        {
            get { return "201810311137596_BirthDayAdded"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
