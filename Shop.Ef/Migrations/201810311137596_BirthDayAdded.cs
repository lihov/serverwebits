namespace Shop.Ef.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BirthDayAdded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Customers", "BirthDay", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Customers", "BirthDay");
        }
    }
}
