namespace Shop.Ef.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class QuantitiyAdded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductOrders", "Quantity", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProductOrders", "Quantity");
        }
    }
}
