﻿using System;
using Shop.Ef.Repository;

namespace Shop.Ef.UoW
{
    public interface IUnitOfWork : IDisposable
    {
        void Save();

        T GetRepository<T>() where T : class;
    }
}
