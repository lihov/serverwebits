﻿using System;
using System.Data.Entity;
using Shop.Ef.IocUtils;

namespace Shop.Ef.UoW
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDependencyResolver _dependencyResolver;

        private const string DbContextParam = "dbContext";

        private readonly DbContext _dbContext;

        public UnitOfWork(IDependencyResolver dependencyResolver)
        {
            _dependencyResolver = dependencyResolver;
            _dbContext = _dependencyResolver.GetService<DbContext>();
        }

        private void AssertContextIsNotNull()
        {
            if (_dbContext == null)
            {
                throw new InvalidOperationException("DbContext already disposed");
            }
        }

        public T GetRepository<T>() where T : class
        {
            AssertContextIsNotNull();
            var result = _dependencyResolver?.GetService<T>(new ConstructorArg(DbContextParam, _dbContext)) 
                ?? throw new InvalidOperationException("Dependency Resolver not initialized");

            if (result == null)
            {
                throw new InvalidOperationException($"Unknown entity type: {typeof(T)}");
            }

            return result;
        }

        public void Save()
        {
            AssertContextIsNotNull();
            _dbContext.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _dbContext.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
