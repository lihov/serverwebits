﻿using System.Data.Entity.ModelConfiguration;
using Shop.Ef.Model;
using Shop.Ef.Helpers;

namespace Shop.Ef.Context.Configurations
{
    public class ProductConfiguration : EntityTypeConfiguration<Product>
    {
        public ProductConfiguration()
        {
            Property(p => p.Name)
                .IsRequired()
                .HasMaxLength(Helper.MaxFieldLength);

            Property(p => p.Price)
                .IsRequired()
                .HasColumnType("Money");
        }
    }
}
