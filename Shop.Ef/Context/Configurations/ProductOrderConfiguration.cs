﻿using System.Data.Entity.ModelConfiguration;
using Shop.Ef.Model;

namespace Shop.Ef.Context.Configurations
{
    public class ProductOrderConfiguration : EntityTypeConfiguration<ProductOrder>
    {
        public ProductOrderConfiguration()
        {
            HasKey(p => new { p.OrderId, p.ProductId });

            HasRequired(p => p.Product)
                .WithMany(p => p.ProductOrders)
                .HasForeignKey(p => p.ProductId);

            HasRequired(p => p.Order)
                .WithMany(o => o.ProductOrders)
                .HasForeignKey(p => p.OrderId);
        }
    }
}
