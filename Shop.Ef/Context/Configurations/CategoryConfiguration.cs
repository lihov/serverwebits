﻿using System.Data.Entity.ModelConfiguration;
using Shop.Ef.Model;
using Shop.Ef.Helpers;

namespace Shop.Ef.Context.Configurations
{
    public class CategoryConfiguration : EntityTypeConfiguration<Category>
    {
        public CategoryConfiguration()
        {
            Property(p => p.Name)
                .IsRequired()
                .HasMaxLength(Helper.MaxFieldLength);
        }
    }
}
