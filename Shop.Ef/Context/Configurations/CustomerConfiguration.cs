﻿using System.Data.Entity.ModelConfiguration;
using Shop.Ef.Model;
using Shop.Ef.Helpers;

namespace Shop.Ef.Context.Configurations
{
    public class CustomerConfiguration : EntityTypeConfiguration<Customer>
    {
        public CustomerConfiguration()
        {
            Property(c => c.Name)
                .IsRequired()
                .HasMaxLength(Helper.MaxShortFieldLength);

            Property(c => c.Email)
                .HasMaxLength(Helper.MaxShortFieldLength);

            Property(c => c.Phone)
                .HasMaxLength(Helper.MaxShortFieldLength);

            Property(o => o.BirthDay).HasColumnType("DateTime2");

            HasRequired(c => c.Order)
                .WithRequiredPrincipal(o => o.Customer);
        }
    }
}
