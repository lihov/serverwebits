﻿using System.Data.Entity.ModelConfiguration;
using Shop.Ef.Model;

namespace Shop.Ef.Context.Configurations
{
    public class ProductCategoryConfiguration : EntityTypeConfiguration<ProductCategory>
    {
        public ProductCategoryConfiguration()
        {
            HasKey(p => new {p.CategoryId, p.ProductId});

            HasRequired(p => p.Product)
                .WithMany(p => p.ProductCategories)
                .HasForeignKey(p => p.ProductId);

            HasRequired(p => p.Category)
                .WithMany(c => c.ProductCategories)
                .HasForeignKey(p => p.CategoryId);
        }
    }
}
