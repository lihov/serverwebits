﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Ef.IocUtils
{
    public interface IDependencyResolver
    {
        T GetService<T>(params ConstructorArg[] constructorArgs);
    }
}
