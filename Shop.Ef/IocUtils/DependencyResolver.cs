﻿using System;
using System.Data.Entity;
using System.Linq;
using Ninject;
using Ninject.Parameters;
using Shop.Ef.Context;
using Shop.Ef.Repository;
using Shop.Ef.Repository.Specific;
using Shop.Ef.UoW;

namespace Shop.Ef.IocUtils
{
    public class DependencyResolver : IDependencyResolver, IDisposable
    {
        private readonly IKernel _kernel;

        private bool _disposed;

        public DependencyResolver(IKernel kernel)
        {
            _kernel = kernel;
            AddBindings();
        }

        private void AssertIsDisposed()
        {
            if (_disposed)
            {
                throw new InvalidOperationException("Invoking method from disposed instance");
            }
        }

        public T GetService<T>(params ConstructorArg[] constructorArgs)
        {
            AssertIsDisposed();
            return constructorArgs.Length == 0 
                ? _kernel.Get<T>() 
                : _kernel.Get<T>(constructorArgs.Select(x => new ConstructorArgument(x.Name, x.Value)).ToArray<IParameter>());
        }

        private void AddBindings()
        {
            _kernel.Bind<DbContext>().To<ShopContext>();
            _kernel.Bind<IUnitOfWork>().To<UnitOfWork>();
            _kernel.Bind<ICategoryRepository>().To<CategoryRepository>();
            _kernel.Bind<ICustomerRepository>().To<CustomerRepository>();
            _kernel.Bind<IOrderRepository>().To<OrderRepository>();
            _kernel.Bind<IProductCategoryRepository>().To<ProductCategoryRepository>();
            _kernel.Bind<IProductOrderRepository>().To<ProductOrderRepository>();
            _kernel.Bind<IProductRepository>().To<ProductRepository>();
        }


        public void Dispose()
        {
            if (_disposed)
            {
                return;
            }
            _kernel?.Dispose();
            _disposed = true;
        }
    }
}
